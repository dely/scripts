#!/bin/bash

killall -9 vlc &>/dev/null;
for ip in `cat /var/run/mumudvb/*streamed* | grep FullyUnscrambled | awk -F  ":" '{print $1}' | sort | uniq | sort -R`; do

(
cvlc udp://@${ip}:1234 --ffmpeg-threads=1 --run-time=2 --sout-deinterlace-mode linear  --rate=1 --video-filter=scene --vout=dummy --aout=dummy --scene-format=jpg --scene-ratio=24 --scene-prefix=${ip} --scene-path=/var/www/images/ --scene-width=900 --scene-replace vlc://quit &>/dev/null
find /var/www/images/ -size 0 -name ${ip}.jpg -exec rm {} \;

sleep 10

if [ -f /var/www/images/${ip}.jpg ]; then
	convert -geometry '200x150 !'  /var/www/images/${ip}.jpg /var/www/images/${ip}_petites.jpg
fi
)
done
