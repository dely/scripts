#!/usr/bin/python
from config import *
import socket

print("""[global]
sap_delay=9
interface=eth0
#ttl=1
""")

for group in multicast.keys():
	for name in multicast[group].keys():
		(chan,dst,port,source)=multicast[group][name]
		print("""[program]
name=%s
address=%s
port=%s
playlist_group=%s
user=crans
machine=%s
""" % (name,dst,port,group, socket.gethostname()))
