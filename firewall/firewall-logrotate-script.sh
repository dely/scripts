#!/bin/bash
#
# Script de sauvegarde régulière des logs firewall vers le nfs
#
# Reste silencieux en cas de succès si la variable d'env $QUIET est non vide

umask 0177

if [[ `hostname` == 'komaz' ]]; then
    logs_src=/var/log/firewall
elif [[ `hostname` == 'odlyd' ]]; then
    logs_src=/var/log/firewall
else
    exit 41
fi
logs_dst="/home/logs/`hostname`"

if [ ! -d "$logs_dst" ]; then
    echo "Destination des logs inexistante"
    exit 42
fi

# tant qu'il existe un fichier, le sauvegarder (<!>)
while ( ls -tr "$logs_src" | grep -E -q 'logall\.log\.[0-9]+\..z2?' ); do
dernier_fichier=$logs_src/$(ls -tr "$logs_src" | grep -E 'logall\.log\.[0-9]+\..z2?' | head -1)

#timestamp=$(bzcat "$dernier_fichier" | head -1 | awk '{print $1}')
#date=$(date -d "1970-01-01 $timestamp sec" +"%Y-%m-%d")
date=$(bzcat "$dernier_fichier"  | head -1 | awk -F 'T' '{print $1}')
[ -n "$QUIET" ] || {
echo install -o root -g root -m 400 "$dernier_fichier" "$logs_dst/logall.log.$date.bz2"
echo rm "$dernier_fichier"
}
install -o root -g root -m 400 "$dernier_fichier" "$logs_dst/logall.log.$date.bz2"
rm "$dernier_fichier"
done
# Fin de boucle </!>
find "$logs_dst" -mtime +365 -delete
