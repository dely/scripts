#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys, re
from time import sleep
from os import system
import threading

if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')
from gestion.ldap_crans import crans_ldap
from gestion import whos
from gestion.annuaires_pg import all_switchs
from gestion.hptools import hpswitch

# mise en forme d'une adresse mac
def format_mac(unformated_mac):
    return str(":".join([i.zfill(2) for i in unformated_mac.split(":")]).lower())


# classe d'interrogation des switchs
class interroge_switch (threading.Thread) :
    def __init__ (self, switch, mac=None, affiche_uplinks=False):
        threading.Thread.__init__(self)
        self.switch  = switch
        self.mac = mac
        self.reponse = None
        self.affiche_uplinks = affiche_uplinks
        self.start()

    def run (self) :
        sw = hpswitch(self.switch)
        prise = None
        iteration = 3
        while (prise==None) & (iteration > 0):
            prise = sw.where_is_mac(self.mac)
            iteration = iteration-1
        if (prise != None): 
            nom=sw.nom(None,prise)
            if self.affiche_uplinks or "uplink" not in nom:
                self.reponse = ("%-10s => prise %-2s : %s" % (self.switch.encode('utf-8').replace('.adm.crans.org',''), str(prise), nom))


# Retourne les infos sur la machine (l'équivalent d'un whos, mais renvoie la
# chaîne de caractères)
def info_machine(mac):
    s = []
    db = crans_ldap()
    machines = db.search("mac=%s" % mac)['machine']
    for m in machines:
        r = whos.machine_details(m)
        # On supprime les couleurs
        r = re.sub('\x1b\[1;([0-9]|[0-9][0-9])m', '', r)
        s.append(r)
    if len(machines) == 0:
        s.append(u"Recherche LDAP de la MAC %s : aucune machine trouvée\n" % mac)
    return u"\n".join(s)


# interrogation des switchs en parallele
def trace_machine(mac, affiche_uplinks=False):
    tableau = []
    # ce code ne fork plus rien du tout depuis e05c4be14c86da88413c598e4c
    for switch in all_switchs(hide=[]):
        tableau.append(interroge_switch(switch, mac, affiche_uplinks))

    for t in tableau:
        t.join()

    resultat = u'Traçage de %s...\n' % mac

    tracage = u''
    for t in tableau:
        if t.reponse:
            tracage += t.reponse + u"\n"
    if tracage == u'':
        tracage = u"Adresse MAC inconnue des switchs\n"

    return (resultat + tracage)


# on interroge les switchs et on fait un whos sur la mac
if __name__ == '__main__':
    mac = format_mac(sys.argv[1])
    affiche_uplinks = len(sys.argv) > 2 and bool(sys.argv[2])
    print trace_machine(mac, affiche_uplinks).encode('utf-8')
