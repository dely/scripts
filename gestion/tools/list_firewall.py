#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/gestion')

from ldap_crans import crans_ldap, decode, AssociationCrans

db = crans_ldap()
machines = db.search('portTCPin=*')['machine'] + db.search('portTCPout=*')['machine'] + db.search('portUDPin=*')['machine'] + db.search('portUDPout=*')['machine']

done = []
txts = []

for m in machines :
    # on vérifie qu'on l'a pas encore traité
    if m.ip() in done :
        continue
    if m.proprietaire().__class__ == AssociationCrans :
        continue
    done.append(m.ip())
    
    # texte pour la machine
    txt =  u'' 
    txt += u'Propriétaire  : %s\n' % m.proprietaire().Nom()
    txt += u'Machine       : %s\n' % m.nom()
    if m.portTCPin() :
        txt += u'ports TCP in  : %s\n' % ' '.join(m.portTCPin())
    if m.portTCPout() :
        txt += u'ports TCP out : %s\n' % ' '.join(m.portTCPout())
    if m.portUDPin() :
        txt += u'ports UDP in  : %s\n' % ' '.join(m.portUDPin())
    if m.portUDPout() :
        txt += u'ports UDP out : %s\n' % ' '.join(m.portUDPout())
    
    txts.append(txt.strip())

print '\n- - - - - - = = = = = = # # # # # # # # = = = = = = - - - - - -\n'.join(txts)
