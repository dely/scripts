#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/gestion')

from ldap_crans import crans_ldap, decode

db = crans_ldap()
txts = []

adhs=db.search('droits=*')['adherent']
droits={}
for adh in adhs:
    for droit in adh.droits():
        droits[droit] = droits.get(droit, []) + [adh]

d=droits.keys()
d.sort()

for droit in  d:
    adhs = droits[droit]
    
    noms = []

    txt =  '%s\n' % droit
    for adh in adhs :
        noms.append(u'%s (%s)' % (adh.Nom(), adh.historique()[0].split(' ', 1)[0].split('/',2)[-1]))

    noms.sort()
    txt += u'   %s' % '\n   '.join(noms)
    
    txts.append(txt)

print '\n- - - - - - = = = = = = # # # # # # # # = = = = = = - - - - - -\n'.join(txts)
