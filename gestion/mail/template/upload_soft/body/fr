Bonjour {{ proprio|name }},

Nous t'informons que ta (tes) machine(s) envoie(nt) une quantité importante de
données vers l'extérieur ({{upload}} Mo en 24 heures).

*Ce message t'est envoyé à titre informatif, il ne te sanctionne pas.*

Il signifie que tu as envoyé plus de {{limite_soft}} Mo au cours des dernières 24 heures.
Cela peut venir du fait que, *par exemple*, tu essaies d'envoyer des fichiers de
grosse taille à l'extérieur de la zone crans, ou encore que tu as fait une
utilisation importante de logiciels envoyant une très grande quantité de petites
données (vidéo-conférence par exemple). Il peut y avoir d'autres raisons.

Si cela continuait, et que tu dépassais la limite acceptable des {{limite_hard}} Mo sur 24
heures, ton débit serait automatiquement fortement limité pour une durée de 24
heures. Il t'appartient donc de surveiller cela et de faire en sorte de ne pas
dépasser cette limite.

Pour plus d'informations, tu peux consulter les pages :
http://wiki.crans.org/VieCrans/DéconnexionPourUpload
http://wiki.crans.org/VieCrans/AvertissementPourUpload

Si tu as des questions, contacte disconnect@crans.org

N.B. : L'upload consiste en l'envoi de données vers des machines n'étant pas
branchées sur le CRANS.

-- 
Disconnect team
