Chers adhérents,

Dans le but de pouvoir nous investir dans le projet Paris-Saclay et ainsi
accompagner le déménagement de l'Ecole Normale Supérieure de Cachan,
l'association doit adapter ses statuts et son règlement intérieur.

Nous avons donc travaillé à leur réécriture, qui vise aussi à clarifier la façon
dont l'association fonctionne, aussi bien en interne que vers les personnes
extérieures à l'association. Ainsi un certain nombre de garanties sont désormais
exprimées explicitement dans les statuts ou le règlement intérieur, en
particulier sur la notion de vie privée (qui est en ce moment au cœur des débats
publics).

Cependant, le temps manque, et nous aimerions pouvoir également mener les
démarches nécessaires dans le cadre du projet Saclay, y compris auprès des
établissements publics concernés. Aussi, nous souhaiterions voter ces
modifications lors d'une Assemblée Générale Extraordinaire qui se tiendrait le
jeudi 9 juillet 2015 à 19h.

Nous avons conscience que ce choix signifie aussi qu'un certain nombre d'entre
vous ne pourront pas nécessairement être présents. Les textes que nous
souhaitons adopter sont en pièce jointe du présent email. Si vous voyez des
choses qui vous semblent problématiques dans l'un d'entre eux, n'hésitez pas à
nous contacter par email (en répondant à celui-ci) pour exprimer votre pensée.

Les précédents statuts et règlement intérieur peuvent être trouvés ici :
https://wiki.crans.org/CransAdministratif/StatutsDuCrans
https://wiki.crans.org/CransAdministratif/R%C3%A8glementInt%C3%A9rieur

Vous pourrez vous rendre au Hall Villon à partir de 9h (heure locale) pour voter
concernant l'adoption des nouveaux textes. L'assemblée se tiendra à
l'amphithéâtre Tocqueville dans le but de dépouiller les votes.

Bien cordialement,

-- 
Les membres actifs du Crans
