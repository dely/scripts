#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
#
# Trigger library, designed to send events messages.
#
# Author    : Pierre-Elliott Bécue <becue@crans.org>
# License   : GPLv3
# Date      : 10/03/2015

"""
This service (event) is designed to receive any modification done on LDAP
database, and to make a correct diff between former and later object in order
to guess which services has to be updated.
"""

# Trigger features
import gestion.config.trigger as trigger_config
from gestion.trigger.host import record_service
from gestion.trigger.services.event import EventTracker, trigger_send # really useful EventList ?

# Clogger
import cranslib.clogger as clogger

logger = clogger.CLogger("trigger", "ack", trigger_config.log_level, trigger_config.debug)

@record_service(ack=False)
def ack(ob_id, service_name):
    """Ack when something has been done.

    Removes the acked thing from
    """

    logger.info("Received message %r…", (ob_id, service_name))

    todo = EventTracker.ack(ob_id, service_name)

    # if todo is None, then we have finished a list, or emptied
    # EventTracker's content.
    if todo is None:
        todo = EventTracker.get_off_record(ob_id)
        logger.info("Emptied one list in the chain %r. Trying to continue. Got %r", ob_id, todo)
    else:
        todo = []

    if todo:
        for msg in todo:
            logger.info("Sending %r on the road \\o/", msg)
            # XXX - uncomment this when in production
            trigger_send(*msg)
