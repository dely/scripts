#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

u"""
Copyright (C) Valentin Samir
Licence : GPLv3

"""
import sys
import time
if '/usr/scripts' not in sys.path:
    sys.path.append('/usr/scripts')

import lc_ldap.objets as objets
import lc_ldap.attributs as attributs

import proprio
from CPS import TailCall, tailcaller, Continue

class Dialog(proprio.Dialog):
    def create_club(self, cont):
        self.dialog.msgbox("todo", width=0, height=0)
        return cont

    def delete_club(self, cont):
        self.dialog.msgbox("todo", width=0, height=0)
        return cont

    def modif_club(self, cont, club=None):
        if club is None:
            club = self.select(["club"], "Recherche d'un club pour modification", disable_field=["Prénom", "Téléphone"], cont=cont)
        self.dialog.msgbox("todo", width=0, height=0)
        return cont(proprio=club)
