#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
"""Ce sont les variables utiles pour les autres scripts du
module"""

OPERSTATUS = {
    1: 'up',
    2: 'down',
    3: 'testing',
    4: 'unknown',
    5: 'dormant',
    6: 'notPresent',
    7: 'lowerLayerDown',
}

ADMINSTATUS = {
    1: 'up',
    2: 'down',
    3: 'testing',
}

ETHSPEED = {
    'HD': {
        0: '5',
        10: '1',
        100: '2',
        1000: '5',
    },
    'FD': {
        0: '5',
        10: '3',
        100: '4',
        1000: '6',
    },
    'AUTO': {
        0: '5',
        10: '7',
        100: '8',
        1000: '9',
    },
}

REV_ETHSPEED = {
    '1': '10 Mbs Half Duplex',
    '2': '100 Mbs Half Duplex',
    '3': '10 Mbs Full Duplex',
    '4': '100 Mbs Full Duplex',
    '6': '1000 Mbs Full Duplex',
    '5': 'auto',
    '7': '10 Mbs auto',
    '8': '100 Mbs auto',
    '9': '1000 Mbs auto',
}
