#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os

from .switch import HPSwitch, SwitchNotFound
from .tools import trace_mac

import gestion.config.snmp as config_snmp

os.environ["MIBS"] = ":".join([mib for mib in config_snmp.PRELOAD_MIBS])
