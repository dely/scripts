# -*- python -*-
# -*- coding: utf-8 -*-

# Fichier anciennement généré par bcfg2, déprécié. Utiliser services.py

adm_only = []

role = {
  'zamok': ['adherents-server'],
  'nat64': ['routeur-nat64'],
  'odlyd': ['wifi-router', 'appt-proxy', 'main-router'],
  'dyson': ['sniffer'],
  'isc': ['appt-proxy'],
  'dhcp': ['appt-proxy'],
  'ovh': ['externe'],
  'soyouz': ['externe'],
  'routeur': ['appt-proxy']
}
