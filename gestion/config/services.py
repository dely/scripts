# -*- coding: utf-8 -*-

from __future__ import print_function

import sys

try:
    from .services_etc import services
except ImportError:
    print("Cannot import /etc/crans/services.py, continuing empty", file=sys.stderr)
    services = {}
