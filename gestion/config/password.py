#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Longueur minimale quand on appelle le script en étant nounou avec
# le bon argument
root_min_len = 4

# Longueur minimale standard
min_len = 9

# Nombre minimal de chiffres requis
min_cif = 1

# Nombre minimal de minuscules requises
min_low = 1

# Nombre minimal de majuscules requises
min_upp = 1

# Nombre minimal d'autres caractères requis
min_oth = 0

# Valeur des majuscules
upp_value = 1

# Valeur des minuscules
low_value = 1

# Valeur des chiffres
cif_value = 1

# Valeur des autres caractères
oth_value = 2
