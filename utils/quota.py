# -*- coding: utf8 -*-
# Reloockage de quota.py, passage à l'intranet 2
# On retire le sudo par défaut, l'user execute lui meme quota
# Ecrit il y a longtemps, "dépoussiéré" et "enrichi" par
# Gabriel Détraz detraz@crans.org

import subprocess

def getFloat(chose):
    chose = chose.replace(',', '.')
    return float(chose)

def getUserQuota(user):
    proc = subprocess.Popen(['sudo', '-n', 'quota', user], stdout=subprocess.PIPE)
    stdoutdata = proc.communicate()[0]
    string_result = stdoutdata.split("\n")
    quotas = []
    for a_line in string_result[2:-1]:
        usage, quota, limite, percentage, fs = a_line.split("\t")
        if "mail" in fs:
            label = u"Quota dans votre boite de réception"
        elif fs == "/home-adh/%s" % (user[0],):
            label = u"Quota dans votre home personnel"
        else:
            label = u"Quota dans %s" % (fs,)
        line_dict = {
            "label" : label,
            "usage" : getFloat(usage),
            "quota" : getFloat(quota),
            "limite" : getFloat(limite),
            "%" : getFloat(percentage),
            "filesystem" : fs, # pourquoi pas ?
        }
        quotas.append(line_dict)
    return quotas

def fake_getUserQuota( userLogin ):
    return [
        {
            '%': 33.9,
            'quota': 390.62,
            'label': u'Dossier personnel (fake)',
            'limite': 585.94,
            'filesystem': '/home',
            'usage': 420.32
        },
        {
            '%': 0.1,
            'quota': 100.00,
            'label': u'Boite de r\xe9ception (fake)',
            'limite': 150.00,
            'filesystem': '/var/mail',
            'usage': 0.06
        }
    ]

