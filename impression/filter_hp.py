#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import sys

DEFAULT_BAC = 7

PUNCH_DRV2PCL_OPTS = {
    "2HolePunchLeft": "LEFT_2PT_DIN",
    "2HolePunchRight": "RIGHT_2PT_DIN",
    "2HolePunchTop": "TOP_2PT_DIN",
    "2HolePunchBottom": "BT_2PT_DIN",
    "4HolePunchLeft": "LEFT_4PT_DIN",
    "4HolePunchRight": "RIGHT_4PT_DIN",
}

STAPLE_DRV2PCL_OPTS = {
  "TopLeft": "LEFT_1PT_ANGLED",
  "TopRight": "RIGHT_1PT_ANGLED",
  "Left": "LEFT_2PT",
  "Right": "RIGHT_2PT",
}

# Commande ghostscript de conversion en PCL
params = ['gs', '-dNOPAUSE', '-dBATCH', '-sDEVICE=pxlmono',
    #'-sPAPERSIZE=A4',
    '-dDuplex=true',
    '-dMediaPosition=%d' % DEFAULT_BAC,
    '-sOutputFile=%stdout%',
    '-sstdout=/dev/null',
    '-f', # To be continued
]

def tweak(source, dest, **opt_):
    """Tweak un fichier pcl (source est un descripteur de fichier en lecture)
    en rajoutant les options PJL nécessaires, et en modifiant le PCL
    pour que les deux premières pages (première feuille) soient prises depuis
    un autre bac"""
    raw = False
    opt = {
        # Nombre de pages depuis le début à piocher depuis bac séparé
        'manual_feed_count': 0,
        'copies': 1,
        'finish': 'STAPLE',
        #'hold': 'STORE',
        'hold': 'OFF',
        'jobname': 'None',
        'staple': 'NONE',
        'hole': 'NONE',
    }
    opt.update(opt_)
    while not raw:
        l = source.readline()
        if l.startswith('@PJL ENTER LANGUAGE'):
            raw = True # Derniere ligne avant les trucs degueux
            dest.write("""@PJL SET JOBNAME = "%(jobname)s"
@PJL SET HOLD = "%(hold)s"
@PJL SET QTY = %(copies)d
@PJL SET PROCESSINGACTION=APPEND
@PJL SET PROCESSINGTYPE="STAPLING"
@PJL SET PROCESSINGOPTION="%(staple)s"
@PJL SET PROCESSINGBOUNDARY=MOPY
@PJL SET PROCESSINGTYPE="PUNCH"
@PJL SET PROCESSINGOPTION="%(hole)s"
@PJL SET PROCESSINGBOUNDARY=MOPY
""" % opt)
        dest.write(l)

    # Entrée manuelle sur les n premières pages. On remplace n fois
    s_string = '\xf8\x25\xc0' + chr(DEFAULT_BAC)
    r_string = '\xf8\x25\xc0\x01'
    count = opt['manual_feed_count']
    #
    while True:
        x = source.read(102400)
        #print "Read 100ko"
        #sys.stdout.flush()
        if not x:
            return
        while count:
            y = x.replace(s_string, r_string, 1)
            if x != y:
                x = y
                count -= 1
            else:
                break
        dest.write(x)

def parse_opt(argv):
    """Parse les arguments à la cups depuis un argv, pour usage dans tweak."""
    backend = argv[0]
    jobid = int(argv[1])
    cupsuser = argv[2]
    jobname = argv[3]
    copies = int(argv[4])
    options = argv[5].split(' ')
    filename = (argv[6:] or ['-'])[0]

    opt = {
        'filename': filename,
        'jobname': jobname,
        'copies': copies,
    }
    opt['jobname'] = argv[3]

 
    # Pour les options supplémentaires
    for entry in options:
        key, value = entry.split("=", 1)
        if key == "HPPunchingOptions":
            opt['hole'] = PUNCH_DRV2PCL_OPTS[value]
        if key == "HPStaplerOptions":
            opt['staple'] = STAPLE_DRV2PCL_OPTS[value]
    return opt

if __name__ == '__main__':
    
    opt = parse_opt(sys.argv)

    proc = subprocess.Popen(params + [opt['filename']],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                stdin=sys.stdin)
    tweak(proc.stdout, sys.stdout, **opt)

