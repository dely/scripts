#!/bin/bash
#
# /usr/lib/cups/backend/2dir
#
# (c) September 2007  Kurt Pfeifle <pfeifle@kde.org>
#                                  <kurt.pfeifle@infotec.com>
#     Network printing consultant Linux/Unix/Windows/Samba/CUPS
#
# License: GNU GPLv2 or GPLv3 (your choice)
# Warranty: None at all; you may need to fix included defects on your own.
#

backend=${0}
jobid=${1}
cupsuser=${2}
jobtitle=${3}
jobcopies=${4}
joboptions=${5}
jobfile=${6}
printtime=$(date +%Y-%b-%d-%H-%M-%S)

# the following messages should appear in /var/log/cups/error_log,
# depending on what "LogLevel" setting your cupsd.conf carries:
echo "INFO: backend=${backend}"       1>&2
echo "INFO: jobid=${jobid}"           1>&2
echo "INFO: cupsuser=${cupsuser}"     1>&2
echo "INFO: jobtitle=${jobtitle}"     1>&2
echo "INFO: jobcopies=${jobcopies}"   1>&2
echo "INFO: joboptions=${joboptions}" 1>&2
echo "INFO: jobfile=${jobfile}"       1>&2
echo "INFO: printtime=${printtime}"   1>&2
echo "EMERG:  This is a \"emergency\" level log message" 1>&2
echo "ALERT:  This is a \"alert\" level log message"     1>&2
echo "CRIT:   This is a \"critical\" level log message"  1>&2
echo "ERROR:  This is a \"error\" level log message"     1>&2
echo "WARN:   This is a \"warn\" level log message"      1>&2
echo "NOTICE: This is a \"notice\" level log message"    1>&2
echo "INFO:   This is a \"info\" level log message"      1>&2
echo "INFO:   This is a 2nd \"info\" level log message"  1>&2
echo "INFO:   This is a 3rd \"info\" level log message"  1>&2
echo "DEBUG:  This is a \"debug\" level log message"     1>&2


# we are free to compose the output filename written by the 2dir backend
# in whatever way we like... However, we must be careful when using the
# $jobtitle part -- the job may originate from a web browser and contain
# slashes and all kinds of illegal or problematic characters. Therefore
# we prefer to radically convert every "weird" character to an underscore
# for our current purpose...

sanitized_jobtitle="$(echo ${jobtitle} | tr [[:blank:]:/%\&=+?\\\\#\'\`\´\*] _)"

# the following lines would do (nearly) the same as the above, but slower
# -- yet better readable (and the above may be in need of some fixing still):
#sanitized_jobtitle="$(echo ${jobtitle} \
#                |tr [:blank:] _ \
#                |tr : _ \
#                |tr \"\ \" _ \
#                |tr / _ \
#                |tr % _ \
#                |tr \' _ \
#                |tr \` _ \
#                |tr \´ _ \
#                |tr \' _ \
#                |tr \& _ \
#                |tr \* _ \
#                |tr \# _ \
#                |tr = _ \
#                |tr + _ \
#                |tr ? _ \
#                |tr \\\\ _ )"
##               # last line to get rid of "backslashes"...

# now for our final job output name:
outname=${jobid}_${printtime}_${sanitized_jobtitle}
# we include the $printtime part to have a uniq name in case the same job
# is tested with different settings to be kept for comparing. It is also
# useful for aligning debug efforts to CUPS' error_log entries.


# we will read the output directory from the printers $DEVICE_URI environment
# variable that should look s.th. like "2dir:/path/to/a/directory" and write
# our printfile as $outname there....

# Now do the real work:
case ${#} in
      0)
         # this case is for "backend discovery mode"
         echo "file 2dir \"KDEPrint Devel Dept.\" \"2dir backend to test CUPS and help KDEPrint development\""
         exit 0
         ;;
      5)
         if [ ! -e ${DEVICE_URI#2dir:} ]; then
           mkdir -p ${DEVICE_URI#2dir:}
           # You may want to change this to 777 to allow you
           # to periodically delete the generated files:
           chmod 755 ${DEVICE_URI#2dir:}
           # WARNING! WARNING! WARNING! Don't use these file permissions
           # on a production print server! This is for development convenience
           # only! WARNING, security risk!
         fi

         # backend needs to read from stdin if number of arguments is 5
         cat - > ${DEVICE_URI#2dir:}/${outname}

         # Make sure everyone can read it
         chmod 644 ${DEVICE_URI#2dir:}/${outname}
         # WARNING! WARNING! WARNING! Don't use these file permissions
         # on a production print server! This is for development convenience
         # only! WARNING, security risk!
         ;;
      6)
         if [ ! -e ${DEVICE_URI#2dir:} ]; then
           mkdir -p ${DEVICE_URI#2dir:}
           # You may want to change this to 777 to allow you
           # to periodically delete the generated files:
           chmod 755 ${DEVICE_URI#2dir:}
           # WARNING! WARNING! WARNING! Don't use these file permissions
           # on a production print server! This is for development convenience
           # only! WARNING, security risk!
         fi

         # backend needs to read from file if number of arguments is 6
         cat ${6} > ${DEVICE_URI#2dir:}/${outname}

         # Make sure everyone can read it
         chmod 644 ${DEVICE_URI#2dir:}/${outname}
         ;;
      1|2|3|4|*)
         # these cases are unsupported
         echo " "
         echo " Usage: 2dir job-id user title copies options [file]"
         echo " "
         echo " (Install as CUPS backend in /usr/lib/cups/backend/2dir)"
         echo " (Use as 'device URI' like \"2dir:/path/to/writeable/directory\" for printer installation.)"
         exit 0
esac

echo 1>&2

# we reach this line only if we actually "printed something"
echo "NOTICE: processed Job ${jobid} to file ${DEVICE_URI#2dir:}/${outname}" 1>&2
echo "NOTICE: End of \"${0}\" run...."                                       1>&2
echo "NOTICE: ---------------------------------------------------------"     1>&2
echo 1>&2
exit 0

################# end "2dir" ##############################################
