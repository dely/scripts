#!/usr/bin/env python
# -*- coding: utf-8 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
"""
 digicode.py

     Fonctions pour controler le digicode du 4@J

 Copyright (c) 2006, 2007, 2008, 2009 by Cr@ns (http://www.crans.org)
"""
import sys
import requests
if '/usr/scripts' not in sys.path:
    sys.path.append("/usr/scripts")

import gestion.secrets_new as secrets_new
from cranslib.deprecated import deprecated

digicode_pass = secrets_new.get("digicode_pass")
# #############################################################
# CONSTANTES
# #############################################################
CODES_SERVERS = ["zamok", "asterisk", "vo"]
CODES_DIR = "/usr/scripts/var/digicode/"
CREATION_LINK = "https://intranet2.crans.org/digicode/create/"
LIST_LINK = "https://intranet2.crans.org/digicode/list/"
CERTIFICATE = "/etc/ssl/certs/cacert.org.pem"

# #############################################################
# FONCTIONS
# #############################################################
# test pour voir si on est bien sur la bonne machine
import socket
if socket.gethostname() not in CODES_SERVERS:
    raise EnvironmentError("La manipulation des codes pour le digicode n'est possible que sur %s" % 
        ', '.join(CODES_SERVERS))

class CommunicationError(Exception):
    pass

def check_status(req):
    status = req.status_code
    if status != 200:
        #TODO un peu plus d'info sur l'erreur
        raise CommunicationError("Bad status from intranet: %d" % status)

# ###############################
# save_code
# ###############################
# enregistre le codes pour user_name sur l'intranet2
#
def save_code(code, user_name):
    """enregistre le codes pour ``user_name``"""
    code = str(code)
    response = requests.post(CREATION_LINK + code, data={'password':digicode_pass, 'user':user_name}, verify=CERTIFICATE, timeout=2)
    check_status(response)
    try:
        code = int(response.content)
    except (TypeError, ValueError):
        raise ValueError(response.content)
    return code


# ###############################
# gen_code
# ###############################
# genere un code aleatoire
# et l'enregistre
#
def gen_code(user_name):
    """On contacte l'intranet 2 pour générer le code et on récupère le résultat"""
    response = requests.post(CREATION_LINK, data={'password':digicode_pass, 'user':user_name}, verify=CERTIFICATE, timeout=2)
    check_status(response)
    return response.content


# ###############################
# list_code
# ###############################
# liste les codes et leur age en secondes
#
def list_code(login=None):
    """
    Renvoie la liste des codes existants.
    La liste est sous la forme [(code, age (en sec), contenu du fichier),...]
    """
    response = requests.post(LIST_LINK + (login if login else ""), data={'password':digicode_pass}, verify=CERTIFICATE, timeout=2)

    check_status(response)
    code_list = []
    for line in response.content.split('\n'):
        if line:
            code_list.append(line.split(','))
    return code_list

def get_codes(login):
    return [code for (code, age, uid) in list_code(login)]

@deprecated
def read_code_file(code):
    """
    Lis le fichier correspondant au code.
    Renvoie le contenu du fichier.
    """
    return "dummy"


# ###############################
# menage
# ###############################
# supprime les codes vieux de plus de 24h
#
@deprecated
def menage():
    """
    Supprime les codes vieux de plus de 24h
    dummy fun
    """
    pass
