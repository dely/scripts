#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script de déconnexion pour mail invalide

Copyright (C) 2009 Michel Blockelet
  inspiré de fiche_deconnexion.py par :
  Xavier Pessoles, Étienne Chové, Vincent Bernat, Nicolas Salles
Licence : GPL v2
"""

import os, sys, time
import subprocess
sys.path.append('/usr/scripts/gestion')
from ldap_crans import crans_ldap
from config import upload
# logging tools
import syslog
def log(x):
    if type(x) == unicode:
        x = x.encode("utf-8")
    syslog.openlog('GENERATE_MAIL_INVALIDE_NOTICE')
    syslog.syslog(x)
    syslog.closelog()

sys.path.insert(0, '/usr/scripts/cranslib')
import utils.exceptions

import locale
locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')


help = """Script de déconnexion pour mail invalide.
Une fiche sera générée pour chaque adhérent.
Chaque adhérent sera déconnecté 2 semaines plus tard si son adresse mail
n'a pas changé.

Usage: mail_invalide.py [adresse mail]...

L'adresse mail peut aussi être un aid précédé d'un tiret.
Exemple:
  mail_invalide.py toto@example.com -42
va s'occuper de l'adhérent ayant toto@example.com comme adresse mail et
l'adhérent ayant l'aid 42."""



def generate_ps(proprio, mail):
    """On génère la feuille d'avertissement et on retourne son emplacement."""
    barcode = "/usr/scripts/admin/mail_invalide/barcode.eps"
    try:
        log('Generate invalid mail notice for %s' % proprio.Nom())
        # Dossier de génération du ps
        dossier = '/usr/scripts/var/mails_invalides'

        # Base pour le nom du fichier
        fichier = time.strftime('%Y-%m-%d-%H-%M') + '-mail-%s' % (proprio.Nom().
            lower().replace(' ', '-'))

        # Création du fichier tex
        format_date = '%A %d %B %Y'
        with open('%s/mail_invalide.tex' % os.path.dirname(__file__), 'r') as tempfile:
            template = tempfile.read()
        template = template.replace('~prenom~', proprio.prenom().encode('utf-8'))
        template = template.replace('~nom~', proprio.nom().encode('utf-8'))
        template = template.replace('~chambre~', proprio.chbre().encode('utf-8'))
        template = template.replace('~mail~', mail.encode('utf-8').replace('_', '\\_'))
        template = template.replace('~fin~',
            time.strftime(format_date, time.localtime(time.time()+14*86400)))

        with open('%s/%s.tex' % (dossier, fichier), 'w') as outtex:
            outtex.write(template)

        # Compilation du fichier latex
        subprocess.check_call(['/usr/bin/pdflatex',
                               '-output-directory='+ dossier,
                               '-interaction', 'nonstopmode',
                               fichier + '.tex',
                              ])
        return '%s/%s.pdf' % (dossier, fichier)

    except Exception, e:
        log('Erreur lors de la génération du ps : ')
        log(str(e))
        log("Values : adherent:%s" % proprio.Nom())
        log(utils.exceptions.formatExc())
        raise

def set_mail_invalide(adherent, mail, a_verifier, a_imprimer):
    if adherent.chbre() in ['????', 'EXT']:
        print u"Chambre de %s : %s, générer la fiche ? [Yn]" % (adherent.Nom().encode('utf-8'), adherent.chbre())
        read = ''
        while read not in ['y', 'n']:
            read = raw_input().lower()
        if read == 'n':
            print u"Chambre de %s : %s, impossible de générer la fiche." % (adherent.Nom().encode('utf-8'), adherent.chbre())
            a_verifier.append(mail)
            return
    
    print "Génération de la fiche pour %s :" % adherent.Nom().encode('utf-8')
    fiche = generate_ps(adherent, mail)
    print fiche
    a_imprimer.append(fiche)
    adherent.blacklist([time.time() + 14 * 24 * 3600,
        '-', 'mail_invalide', "Mail invalide"])
    adherent.save()

if __name__ == "__main__":
    if '--help' in sys.argv or '-h' in sys.argv or len(sys.argv) < 2:
        print help
        sys.exit(0)

    db = crans_ldap()

    # On fait la liste des .forwards dans les homes
    print " * Lecture des .forward ..."
    forwards = {}
    for uid in os.listdir('/home'):
        # Certains homes ne sont pas accessibles
        try:
            files = os.listdir('/home/%s' % uid)
        except OSError as e:
            #print "Home non-accessible : %s" % e
            pass
        else:
            if ".forward" in files:
                # Même si le home n'est pas inaccessible, le .forward peut l'être
                try:
                    redirection = open('/home/%s/.forward' % uid, 'r').readline().strip()
                except IOError as e:
                    #print ".forward non-accessible : %s" % e
                    pass
                else:
                    forwards[redirection] = uid

    a_imprimer = []
    a_verifier = []

    for adresse in sys.argv[1:]:
        # Est-ce un aid ?
        if adresse[0] == '-':
            print " * Recherche de aid=%s ..." % adresse[1:]
            res = db.search("aid=%s" % adresse[1:], 'w')['adherent']
            if len(res) == 0:
                print "*** Erreur : aucun résultat pour aid=%s" % adresse[1:]
                a_verifier.append(adresse)
            elif len(res) > 1:
                print "*** Erreur : plusieurs résultats pour aid=%s :" % adresse[1:]
                for adh in res:
                    print adh.Nom()
                a_verifier.append(adresse)
            else:
                adherent = res[0]
                set_mail_invalide(adherent, adherent.email(), a_verifier, a_imprimer)
            continue

        print " * Recherche de %s ..." % adresse
        # Est-ce un .forward ?
        if forwards.has_key(adresse):
            res = db.search("uid=%s" % forwards[adresse], 'w')['adherent']
            if len(res) == 0:
                print "*** Erreur : aucun résultat pour uid=%s" % forwards[adresse]
                a_verifier.append(adresse)
            else:
                adherent = res[0]
                set_mail_invalide(adherent, adresse, a_verifier, a_imprimer)
                continue

        # Est-ce une adresse mail sans compte Cr@ns ?
        res = db.search("mail=%s" % adresse, 'w')['adherent']
        if len(res) == 0:
            print "*** Erreur : aucun résultat pour %s" % adresse
            a_verifier.append(adresse)
        elif len(res) > 1:
            print "*** Erreur : plusieurs résultats pour %s :" % adresse
            for adh in res:
                print adh.Nom()
            a_verifier.append(adresse)
        else:
            adherent = res[0]
            set_mail_invalide(adherent, adherent.email(), a_verifier, a_imprimer)

    if len(a_verifier) + len(a_imprimer) > 0:
        print ''
        print '***** Résultats *****'
        if len(a_verifier) > 0:
            print ' * Adresses mail à vérifier :'
            print ','.join(a_verifier)
        if len(a_imprimer) > 0:
            print ' * Fiches à imprimer :'
            for fiche in a_imprimer:
                print fiche
