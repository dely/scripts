Encoding: utf-8
Subject: [CRANS] Compte inactif depuis plus d'un mois

Bonjour %(nom)s,

Si tu t'es connecté(e) (par ssh, pop, imap ou webmail) dans les trois
dernières heures, tu peux ignorer ce message.

Tu reçois ce mail parce que tu as un compte sur le serveur des adhérents du
CRANS, et que tu ne t'es pas connecté(e) depuis le %(date)s
(ou tu ne t'es jamais connecté(e)), ce qui fait plus d'un mois.

Tu as %(mail)d nouveau(x) mail(s) dans ta boîte de réception. Nous te
rappelons que la lecture des mails sur ton adresse du CRANS est obligatoire,
car c'est par ce moyen que nous communiquons avec toi.

Si tu lis ce mail, c'est probablement que tu t'es connecté(e) récemment.
Dans ce cas, tu peux ignorer ce message.

Si nous ne détectons toujours aucune activité dans le mois qui suit, cela
voudra dire que tu ne lis pas tes mails et tu pourrais être sanctionné(e).

Nous te signalons qu'il est possible de rediriger tes mails. Pour plus
d'informations, consulte http://www.crans.org/VieCrans/G%%C3%%A9rerSesMails.

Nous te prions de ne pas répondre à ce mail, sauf pour nous signaler une
anomalie ou confirmer un abandon définitif du compte.

-- 
Script de détection des comptes inactifs
