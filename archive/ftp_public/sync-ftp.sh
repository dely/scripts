#! /bin/sh
#
# Script de mise � jour automatique du FTP public du Cr@ns.
#
# Copyright (C) Augustin Parret-Fr�aud
# Licence : GPLv2
#

# TODO : * creer un fichier de logs journalier (modifier la config de rsync 
#        devrait suffire)
#        * int�ger Chronomium, Dynebolic et Kaella (ie. trouver un 
#        serveur rsync qui le fasse ou utiliser autre chose (ftpmirror ?)

# Chemin du FTP
PATHFTP="/pubftp/august/"

# Distributions � g�rer (nb : le nom du r�pertoire doit �tre
# le m�me que celui indiqu� dans la liste ci-apr�s).
# Debian, Fedora, Gentoo, Knoppix, Ubuntu
DISTRIB="Knoppix"
#DISTRIB="Debian Fedora Gentoo Knoppix Ubuntu"

# Variables diverses
LOGFILE="sync-ftp.log"
LOGRSYNC="/localhome/parret-freaud/log/rsync"
LOGTEMP="/localhome/parret-freaud/log/temp"
LOGMAIL="/localhome/parret-freaud/log/mail"
RSYNCOPT="--recursive --links --hard-links --times --verbose --delete-after"
ENTMAIL1="Liste des fichiers mis � jour sur le ftp public\n\
===============================================\n\n"
ENTMAIL3="-- \nMessage g�n�r� par sync-ftp.sh"

# Fichiers en �criture pour le groupe (�a ne fonctionne pas, � creuser ...)
cd $PATHFTP
umask 002

# Boucle principale

echo -e $ENTMAIL1 > $LOGMAIL

for DIST in $DISTRIB; do

  case $DIST in

    Debian)
    # Adresse du miroir
    MIRROR="rsync://ftp.de.debian.org/debian-cd"
    # Architectures � exclure (� choisir parmi :
    # alpha arm hppa i386 ia64 m68k mips mipsel powerpc s390 sparc)
    ARCH_EXCLUDE="alpha arm hppa ia64 m68k mips mipsel s390"
    # Fichiers/repertoires � exclude pour ces architectures
    # de la forme "--exclude foo1$ARCH/ --exclude foo2$ARCH/ ... "
    #ARCH_FR_EXCL="--exclude ${ARCH} "
    # Exclusions suppl�mentaires
    EXCLUDE="--exclude project --exclude bt-cd --exclude jigdo-cd \
    --exclude jigdo-dvd --exclude bt-dvd --exclude source \
    --exclude HEADER.html --exclude trace --exclude mkimage.log "
    # A virer une fois les tests finis
    for ARCH in $ARCH_EXCLUDE; do
      EXCLUDE=$EXCLUDE"--exclude $ARCH "
    done
    PATHLOCAL=$PATHFTP"$DIST/"
    ENTMAIL2="$DIST ($PATHLOCAL)\n======\n"
    TAIL=15 # Pour le formattage du mail
    ;;

    Fedora)
    # Adresse du miroir
    MIRROR="rsync://ftp.uvsq.fr/fedora/"
    # Architectures � exclure (� choisir parmi :
    # i386, x86-64)
    ARCH_EXCLUDE=""
    # Exclusions suppl�mentaires
    EXCLUDE="--exclude development --exclude test --exclude updates \
    --exclude source --exclude debug --exclude os --exclude SRPMS \
    --exclude *-rescuecd.iso --exclude *-disc* "
    for ARCH in $ARCH_EXCLUDE; do
      EXCLUDE=$EXCLUDE"--exclude $ARCH "
    done
    PATHLOCAL=$PATHFTP"$DIST/"
    ENTMAIL2="$DIST ($PATHLOCAL)\n======\n"
    TAIL=3
    ;;

    Gentoo)
    # Adresse du miroir
    MIRROR="rsync://ftp.belnet.be/gentoo/releases/"
    # Architectures � exclure (� choisir parmi :
    # alpha, amd64, hppa, ia64, ppc, ppc64, sparc, x86)
    ARCH_EXCLUDE="alpha hppa ia64 ppc ppc64 sparc"
    # Exclusions suppl�mentaires
    EXCLUDE="--exclude historical --exclude mips --exclude stages \
    --exclude packagecd --exclude snapshots --exclude livecd "
    for ARCH in $ARCH_EXCLUDE; do
      EXCLUDE=$EXCLUDE"--exclude $ARCH "
    done
    PATHLOCAL=$PATHFTP"$DIST/"
    ENTMAIL2="$DIST ($PATHLOCAL)\n======\n"
    TAIL=21
    ;;

    Knoppix)
    # Adresse du miroir
    MIRROR="rsync://ftp.belnet.be/packages/knoppix/"
    # Architectures � exclure (� choisir parmi :
    # alpha, amd64, hppa, ia64, ppc, ppc64, sparc, x86)
    ARCH_EXCLUDE=""
    # Exclusions suppl�mentaires
    EXCLUDE="--exclude contrib --exclude docs --exclude dvd \
    --exclude knoppix-cover --exclude knoppix-customize  \
    --exclude knoppix-dvd --exclude packages-dvd.txt \
    --exclude knoppix-vortrag-als2000 --exclude md5-old --exclude qemu-0.8.1 \
    --exclude qemu-0.8.1-windows-README.txt --exclude *DE* "
    for ARCH in $ARCH_EXCLUDE; do
      EXCLUDE=$EXCLUDE"--exclude $ARCH "
    done
    PATHLOCAL=$PATHFTP"$DIST/"
    ENTMAIL2="$DIST ($PATHLOCAL)\n=======\n"
    TAIL=21
    ;;

    Ubuntu)
    # Adresse du miroir
    MIRROR="rsync://ftp.oleane.net/ubuntu-cd/"
    # Architectures � exclure (� choisir parmi :
    # amd64 i386 powerpc sparc)
    ARCH_EXCLUDE="amd64 powerpc sparc"
    # Fichiers/repertoires � exclude pour ces architectures
    # de la forme "--exclude foo1$ARCH/ --exclude foo2$ARCH/ ... "
    #ARCH_FR_EXCL="--exclude ${ARCH} "
    # Exclusions suppl�mentaires
    EXCLUDE="--exclude cdicons --exclude edubuntu --exclude favicon.ico \
    --exclude FOOTER.html --exclude HEADER.html --exclude jigit \
    --exclude releases --exclude ubuntu-server --exclude .trace \
    --exclude .htaccess --exclude .manifest "
    # A virer une fois les tests finis
    EXCLUDE=$EXCLUDE"--exclude kubuntu* --exclude xubuntu* --exclude ubuntu* "
    for ARCH in $ARCH_EXCLUDE; do
      EXCLUDE=$EXCLUDE"--exclude *-$ARCH.* "
    done
    PATHLOCAL=$PATHFTP"$DIST/"
    ENTMAIL2="$DIST ($PATHLOCAL)\n======\n"
    TAIL=8
    ;;

  esac

  umask 002
  rsync $RSYNCOPT $EXCLUDE $MIRROR $PATHLOCAL > $LOGRSYNC
  ERRC=$?
  cat $LOGRSYNC
  # En cas d'erreur, on notifie avec le code de sortie.
  if [ $ERRC != 0 ]; then
    echo "Erreur $ERRC"
    echo -e $ENTMAIL2 >> $LOGMAIL
    echo -e "Erreur, le processus rsync a retourn� le code $ERRC.\n\n" >> $LOGMAIL
  else
    # Si une mise � jour a �t� effectu�e, on notifie.
    tail -n +$TAIL $LOGRSYNC > $LOGTEMP
    # Sortie de test
    cat $LOGTEMP
    STATUS=$(cat $LOGTEMP | wc -l)
    if (($STATUS > 2)); then
      echo -e $ENTMAIL2 >> $LOGMAIL
      cat $LOGTEMP >> $LOGMAIL
      echo -e "\n" >> $LOGMAIL
    fi
  fi

#echo $STATUS

done

# Fonction d'envoi du mail de notification
mail_ftp() {
  local FROM="From:FTP Public (Sila)<roots@crans.org>"
  local SUBJECT="Notification de synchronisation du FTP"
  local RECIPIENT="<roots@crans.org>"
  local XMAILER="X-Mailer:Bash sync-ftp (by August')"
  echo $1 | mail -a "$FROM" -a "$XMAILER" -s "$SUBJECT" $RECIPIENT < $1
}

# Envoi du mail de notification
LENGTH_MAIL=$(cat $LOGMAIL | wc -l)
if (($LENGTH_MAIL > 4)); then
  echo -e $ENTMAIL3 >> $LOGMAIL
  mail_ftp $LOGMAIL
fi

echo $LENGTH_MAIL

# Nettoyage des fichiers temporaires
rm -f $LOGRSYNC
rm -f $LOGTEMP
rm -f $LOGMAIL

exit 0
