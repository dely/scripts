#! /usr/bin/env python
# -*- encoding: iso-8859-15 -*-


import cPickle,re

exempts = { 'dst' : [], 'src_dst' : {} }

"""
exempts['dst'] : liste de r�seaux (exprim�s en regex) vers lesquels le traffic est exempt�
exempts['src_dst'] [ IP ] : idem, mais seulement en provenance de IP.

"""

exempts['dst'].append( '138\.231\..*' )   # * => *.ens-cachan.fr

exempts['src_dst']['138.231.149.10'] = ['134\.157\.96\.216']		            # rivendell.wifi.crans.org => *.ccr.jussieu.fr
exempts['src_dst']['138.231.141.187'] = ['129\.104\.17\..*', '134\.157\.96\.216' ]  # barad-dur.crans => *.polytechnique.fr et *.ccr.jussieu.fr
exempts['src_dst']['138.231.136.7'] = ['195\.221\.21\.36']                          # egon => ftp.crihan.fr pour rsync mirroir debian/fedor
exempts['src_dst']['138.231.143.62'] =['193\.49\.25\.152' , '138\.195\.34\..*' ]    # ogre => centrale / labo fast (psud)
exempts['src_dst']['138.231.140.173'] =['195\.220\.131\.33' , '195\.220\.133\.98' ] # duckien => rebol.ephe.sorbonne.fr oss.ephe.sorbonne.fr, 28/1/2005 -- Bilou
exempts['src_dst']['138.231.137.230'] =['129\.175\.100\.221' ] # helene => orsay
exempts['src_dst']['138.231.136.7'] =['138\.195\..*' ] # egon => centrale paris
exempts['src_dst']['138.231.139.106'] =['138\.195\.74\..*' ] # schuss => centrale paris
exempts['src_dst']['138.231.139.106'] =['138\.195\.75\..*' ] # schuss => centrale paris
exempts['src_dst']['138.231.150.106'] =['157\.99\.164\.27' ] # sayan-ftp.wifi => chile.sysbio.pasteur.fr

def compileRegs( exempts) : 
    L = []
    for s in exempts['dst'] :
        L.append( re.compile(s) )
    exempts['dst'] = L
    for k in exempts['src_dst'].keys() :
        L = []
        for s in exempts['src_dst'] [k] :
            L.append( re.compile(s) )
        exempts['src_dst'] [k] = L


compileRegs( exempts )
fd=open("/tmp/exempts.pickle","wb")
cPickle.dump(exempts, fd)
