#!/bin/bash

cd /usr/scripts/var/dnssec

if [[ $1 == "" ]]
    then echo "Usage: $0 nom_de_la_zone"
         echo "Exemple: $0 wifi.crans.org"
         exit
fi

rm zone_$1

cat K$1*.key >> zone_$1

chmod 664 zone_*

