#!/bin/sh

# Ce script reconstitue la base mkinfos et tout ce qui va avec
# (liste des pages html, etc...)

cd /usr/scripts/listepagesperso

# mkinfos (g�n�ration de la base)
python mkinfos/mkinfos.py > /var/local/pages_persos/erreurs.txt
cp /tmp/infos.db /var/local/pages_persos/

# mkhtml (construction de la page html � partir de base)
python mkinfos/mkhtml.py > /home/httpd/html/pages-index.html
