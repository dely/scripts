#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

"""Le programme mkinfos.

Ce programme cr�e une base /tmp/infos.db qui reflete l'�tat des comptes
(fichiers .info, .plan et pr�sence de .www/index.html)

La vocation de ce programme est de tourner au moins une fois toutes les 
24 heures.
"""

import BuildDb
import InfoDb
import CompteRec

def DisplayError(self, login, errorlines):
	""" Affiche une erreur de compilation du .info de 'login'.
	
	errorlines contient un tableau de chaines de caracteres.
	Cette routine est une redefinition de CompteRec.CCompte.DisplayError
	"""
	
	msg = [login]
	msg = msg + ["Il y a une erreur dans ton fichier .info ! "]
	msg = msg + [""]
	msg = msg + ["voila le texte de l'erreur : "]
	
	for i in errorlines : 
	    msg = msg + [i]
	msg = msg + [""]
	msg = msg + ["          -- le d�mon 'mkinfos' "]
	
	for i in msg:
	    print i


print "Cr�ation de la base temporaire /tmp/infos.db ... "
CompteRec.CCompte.DisplayError = DisplayError

BuildDb.GrabInfos(InfoDb.CInfoDb("/tmp/infos.db","n"))
