# -*- mode: python; coding: utf-8 -*-
#
# DebAutoPkg.py
# -------------
# Copyright : (c) 2008, Jeremie Dimino <jeremie@dimino.org>
# Licence   : BSD3

'''Plugin fournissant une régle par défault pour les paquets'''

__all__ = ["DebAutoPkg"]

import Bcfg2.Server.Plugin

class DebAutoPkg(Bcfg2.Server.Plugin.Plugin,Bcfg2.Server.Plugin.Generator):
    name = 'DebAutoPkg'
    __version__ = '1.0'
    __author__ = 'dimino@crans.org'

    def HandlesEntry(self, entry, metadata):
        # Ce plugin fournit une règle pour toute entrée de type
        # "Package"
        return entry.tag == 'Package'

    def HandleEntry(self, entry, metadata):
        entry.attrib['version'] = 'auto'
        entry.attrib['type'] = 'deb'
