from sqlobject import *
from datetime import datetime

from local import Local

class Proprio(SQLObject):

    id_ldap = IntCol(unique=True)
    """ L'aid/cid est uniquement la pour la retrocompatibilite avec LDAP
    Attention : dans LDAP, le meme aid peut etre utilise par deux adherents successivement """
    categorie = UnicodeCol()
    """ Categorie : club ou adherent """
    
    nom = UnicodeCol()
    prenom = UnicodeCol()
    titre = StringCol() 
    """ M, Mmme, Mlle """
    naissance = DateCol()
    
    adresse1 = UnicodeCol()
    adresse2 = UnicodeCol()
    code_postal = StringCol()
    ville  = UnicodeCol()
    pays = UnicodeCol()

    telephone = StringCol()
    email = StringCol()
    
    etablissement = UnicodeCol()
    annee_etudes = UnicodeCol()
    filiere = UnicodeCol()
    
    responsable = ForeignKey('Proprio')
    
    fictif = BoolCol()
    remarques = UnicodeCol()
    
    
    #A faire plutot en SQL ?
    #def sync(self):
    #    if not self._SO_createValues:
    #        return
    #for champ in self._SO_createValues.keys():
    #    diff = '%s -> %s'
    #        hist = HistProprio(responsable = None, date = datetime.now(), \
    #               cible = self, champ = champ, \
    #               valeur = self._SO_createValues[champ])
    #    SQLObject.sync(self)
        
    
class HistProprio(SQLObject):
    date = DateTimeCol()
    responsable = ForeignKey('Proprio', notNone=False)
    cible = ForeignKey('Proprio')
    champ = StringCol()
    valeur = StringCol()
    