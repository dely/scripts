#! /usr/bin/env python

import re, commands
import cranslib.utils.logs
log = cranslib.utils.logs.getFileLogger('autologout')

# pour chaque ligne du w
for w in commands.getoutput("w -h").split('\n') :
    if not w : continue
    # on splite
    w = w.split()

    if w[0] in ['cohen','segaud']:
        continue
    
    # on verifie que c'est une connection du bde
    hosts = ['bde.crans.org','cableur.crans.org','cableuse.crans.org','venus.crans.org']
    if w[2] not in [ h[0:16] for h in hosts ] :
        continue
    
    # on verifie qu'on a depase le timeout
    if re.match('^\d*\.\d*s$', w[4]) or re.match('^[0-4]:\d*m$', w[4]) :
        continue
    
    # on reccuperre les processus s le tty
    ps = commands.getoutput('ps auwwx | grep "%s" | head -n 1' % w[1] ).split()
    
    # on verrifie que c'est le bon user
    if ps[0] != w[0] :
        continue
    
    # on verifie qu'on a pas de tty
    if ps[6] != '?' :
        continue
    
    # on tue le process
    commands.getoutput('kill %s' % ps[1] )
    log.info("%s a ete deconnecte" % ps[0])
    #print ps
