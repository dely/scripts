#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

import cgi, sys, os, sha, time, re
from html import html
from session import session
from utilisateurs import users

sys.path.append('/usr/scripts/gestion')
from ldap_crans import crans_ldap
db = crans_ldap()

######################################################
# fontion pour logguer un truc
def log (message) :
    f = open('/var/log/wifiweb.log','a+')
    f.write( time.strftime('%d/%m/%Y %H:%M:%S',time.localtime(time.time())) + ' ' + message + '\n')
    f.close()

######################################################
# initialisation des variables

# initialisation de la page html
page = html()
page.sendheaderstobrowser()
page.titre("Gestion des bornes wifi")

# r�cup�ration des donn�es du formulaire
form = cgi.FieldStorage()
sid = form.getvalue('sid')
action = form.getvalue('action','auth')

# url
try :
    url = os.environ['HTTP_REFERER']
    if '?' in url :
        url = url.split('?')[0]
except :
    url = ''

# cr�ation de la session
try :
    sess = session(sid)
except :
    sess = session()
sid = sess.sid

######################################################
# quelques fontions html

def message (texte, color='black'):
    return '<font color="%s">%s</font><br>' % (color,texte)

def message_ok (texte) :
    return message (texte,'green')

def message_nok (texte) :
    return message (texte,'red')
    
######################################################
# quelques variables html

def bouton (url='', sid='', action='', caption='') :
    txt = ''    
    if url :
        txt += '<form action="%s" method="POST">\n' % url
    else :
        txt += '<form method="POST">\n'
    if sid :
        txt += '<input type="hidden" name="sid" value="%s">\n' % sid
    if action :
        txt += '<input type="hidden" name="action" value="%s">' % action
    txt += '<input type="submit" value="%s">\n</form>' % caption
    return txt

bouton_quitter = bouton (url, sid, 'logout', 'Quitter')
bouton_menu    = bouton (url, sid, 'index', 'Menu principal')
bouton_retour  = bouton ('javascript:history.go(-1)', '', '', 'Retour')

######################################################
# authentification

if action == 'valid-auth' :
    sess.data['user'] = form.getvalue('user','')
    sess.data['password'] = sha.new(form.getvalue('password','')).hexdigest()
    if users.has_key( sess.data['user'] + ':' + sess.data['password'] ) :
        log(sess.data['user'] + ' s\'est connect�' )
    action = 'index'

if not sess.data.has_key('user') or not sess.data.has_key('password') :
    action = 'auth'

if action != 'auth' :
    
    if users.has_key( sess.data['user'] + ':' + sess.data['password'] ) :
        # droits de l'utilisateur
        #########################

        # construction de la liste des bornes modifiables
        bornes_modifiables = []
        if len(users[ sess.data['user'] + ':' + sess.data['password'] ]) :
            for lieu in users[ sess.data['user'] + ':' + sess.data['password'] ] :
                for borne in db.search('canal=*&info=<lieu>%s*' % lieu )['machine'] :
                    nom = borne.nom().encode('iso-8859-15').split('.')[0]
                    if nom not in bornes_modifiables :
                        bornes_modifiables.append(nom)
        
    else :
        # erreur d'authentification
        ###########################
        
        log(sess.data['user'] + ' erreur d\'authentification' )
        page.add('<font color="red">Erreur d\'authentification !</font><br><br>')
        action = 'auth'

######################################################
# page : authentification

if action == 'auth' :
    page.add("""<center>
<form method="POST">
<input type="hidden" name="sid" value="%s">
<input type="hidden" name="action" value="valid-auth">
<table>
<tr><td>Utilisateur : </td><td><input type="text" name="user">
<tr><td>Mot de passe : </td><td><input type="password" name="password"><br></td></tr>
<tr><td collspan="2" align="center"><input type="submit" value="Valider"></td></tr>
</table>
</form>
</center>
""" % sid )
    action = ''

######################################################
# d�sactivation d'un borne

if action == 'desactive' and bornes_modifiables :
    if form.getvalue('borne','') in bornes_modifiables :
        log(sess.data['user'] + ' a d�sactiv� %s' % form.getvalue('borne','') )
        page.add('<font color="blue">La borne <b>%s</b> sera d&eacute;sactiv&eacute;e dans quelques instants</font><br><br>' % form.getvalue('borne','') )
        borne = db.search('host=%s.wifi.crans.org' % form.getvalue('borne',''), 'w' )['machine'][0]
        if int(borne.puissance()) > 0 :
            borne.puissance(-int(borne.puissance()))
        borne.save()
        action = 'liste-bornes'
    else :
        log(sess.data['user'] + ' a tent� de d�sactiver %s' % form.getvalue('borne','') )
        action = 'erreur-droits'

######################################################
# activation d'un borne

if action == 'active' and bornes_modifiables :
    if form.getvalue('borne','') in bornes_modifiables :
        log(sess.data['user'] + ' a activ� %s' % form.getvalue('borne','') )
        page.add('<font color="blue">La borne <b>%s</b> sera r&eacute;activ&eacute;e dans quelques instants</font><br><br>' % form.getvalue('borne','') )
        borne = db.search('host=%s.wifi.crans.org' % form.getvalue('borne',''),'w' )['machine'][0]
        if int(borne.puissance()) < 0 :
            borne.puissance(int(borne.puissance().replace('-','')))
        borne.save()
        action = 'liste-bornes'
    else :
        log(sess.data['user'] + ' a tent� d\'activer %s' % form.getvalue('borne','') )
        page.add('<font color="red">Vous n\'&ecirc;tes pas authoris&eacute; � modifier la borne <b>%s</b></font><br><br>' % form.getvalue('borne','') )
        action = 'erreur-droits'
    
######################################################
# page : liste des bornes

if action == 'liste-bornes' and bornes_modifiables :
    page.sous_titre('Liste des bornes')

    for b in bornes_modifiables :
        try :
            borne = db.search('host=%s.wifi.crans.org' % b)['machine'][0]
        except :
            log('borne non existante : %s' % b)
            continue
        
        # formulaire
        page.add('<form method=\"POST\">')
        page.add('<input type="hidden" name="sid" value="%s">' % sid )
        page.add('<input type="hidden" name="borne" value="%s">' % b)
        
        # titre
        if '-' in borne.puissance() :
            # r�activation
            page.add('<b><u>%s</u></b> <font color="red">(borne d&eacute;sactiv&eacute;e)</font>' % borne.Nom().encode('iso-8859-15'))
            page.add('<input type="hidden" name="action" value="active">')
        else :
            # d�sctivation
            page.add('<b><u>%s</u></b> <font color="green">(borne activ&eacute;e)</font>' % borne.Nom().encode('iso-8859-15'))
            page.add('<input type="hidden" name="action" value="desactive">')
        page.add('<br>')
        
        # commentaires
        page.add('<table><tr><td width=\"20\">&nbsp;</td><td>')
        page.add('<br>'.join( filter(lambda x : re.match("^\<.*\>", x) == None, borne.info() ) ).encode('iso-8859-15'))
        page.add('</td></tr></table>')
        
        # bouton de validation
        if '-' in borne.puissance() :
            page.add('<input type="submit" value="R&eacute;activer %s">' % b)
        else :
            page.add('<input type="submit" value="D&eacute;sactiver %s">' % b)
    
        # fin du formulaire
        page.add('</form>')

    # menu de fin de page
    page.add( "<center><table><tr><td>%s</td><td>%s</td><td>%s</td></tr></center>" % ( bouton(url,sid,'liste-bornes','Actualiser'), bouton_menu, bouton_quitter ) )
    action = ''

######################################################
# page : erreur de droits

# si on a encore quelque chose � afficher c'est qu'il y  une erreur
# on colle ca sur la faute des droits
if action not in [ 'index', 'logout', '' ] :
    page.add("<font color=\"red\">Vous n'avez pas le droits d'effectuer cette op&eacute;ration</font><br><br>\n")
    page.add('<center><a href="javascript:history.go(-1)">Retour</a></center><br>')
    action = ''

######################################################
# page : index

if action == 'index' :
    # menu principal
    page.sous_titre("Menu principal")
    if bornes_modifiables :
        page.add('<a href="?sid=%s&action=liste-bornes">Activation/d&eacute;sactivation d\'une borne</a><br>' % sid )
    # menu de bas de page
    page.add("<center>%s</center>" % bouton_quitter )

######################################################
# page : logout

if action == 'logout' :
    log(sess.data['user'] + ' s\'est d�connect�' )
    page.sous_titre('Session ferm�e...')
    sess.destroy()

######################################################
# fin du script
page.sendtobrowser()
