#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
Script pour placer les gens qui font du routeur advertisement sur le vlan
isolement
© Olivier Huber
License : GPLv2
"""

import os
import sys
sys.stdout = open('/tmp/badboys', 'a')
sys.stderr = open('/tmp/badboys', 'a')
import time
sys.path.append('/usr/scripts/gestion')
from ldap_crans import crans_ldap, is_actif
sys.path.append('/usr/scripts/utils')
from chambre_on_off import chambre_on_off
from time import sleep
if __name__ == '__main__':
    Mac_ra = os.getenv('SOURCE_MAC')
    print Mac_ra
    with open('/tmp/badboys', 'a') as f:
        print >>f, Mac_ra
    Ldap = crans_ldap()
    try:
        Machine = Ldap.search('mac=%s' % Mac_ra, 'w' )['machine'][0]
    except IndexError:
        print u"La machine avec la mac %s n'est pas declarée !" % Mac_ra
        sys.exit(1)

    Host = Machine.nom()
    Prop = Machine.proprietaire()
    Bl = Prop.blacklist()
    Bl_ipv6 = [ x for x in Bl if 'ipv6_ra' in x ]
    for bl in Bl_ipv6:
        if is_actif(bl):
            # L'adhérent est déjà blacklisté
            print "déjà blacklisté !!!!"
            sys.exit(1)
    else:
        # L'adhérent n'est pas encore blacklisté
        Prop.blacklist(['now', '-', 'ipv6_ra', Host])
        print "ur a bad guy"
        Prop.save()
        Chbre = Machine.proprietaire().chbre()
        chambre_on_off(Chbre, 'off')
        time.sleep(5)
        chambre_on_off(Chbre, 'on')
    print str(Host), str(Prop), Machine.proprietaire().chbre()
    sys.exit(0)
