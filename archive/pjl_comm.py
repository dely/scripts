#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-

"""Classe de communication avec l'imprimante
HP9500"""

import socket

class hp9500:
    "Classe de communication avec la HP9500"
    #\x1B est le caractere d'�chappement �chap.
    UEL="\x1b%-12345X"
    __douille=None
    def __init__(self,hostname="laserjet.adm.crans.org",port=9100):
        """Appelle � l'initalisation
        hostname : nom d'h�te de l'imprimante
        port : le port de communication avec l'imprimante
        """
        self.hostname=hostname
        self.port=port

    def cx_open(self):
        """Ouvre la connexion vers l'imprimante"""

        if not self.__douille:
            self.__douille = socket.socket(socket.AF_INET, socket.SOCK_STREAM,socket.IPPROTO_TCP)
            self.__douille.connect((self.hostname, self.port))
            #delai d'attente pour la r�ception des donn�es
            self.__douille.settimeout(10)
            self.__douille.sendall(self.UEL+"@PJL\n")
        
    def cx_close(self):
        """Ferme la connexion vers l'imprimante"""

        if self.__douille:
            self.__douille.sendall(self.UEL+"\n")
            self.__douille.close()
            self.__douille=None

    def pjl_command(self,command):
        """Cette fonction envoie la commande command � l'imprimante
        Elle doit �tre une commande pjl sans @PJL devant

        Param�tres :
        command : la commande � envoyer � l'imprimante

        """

        if not self.__douille:
            self.cx_open()

        command=str(command)
        command="@PJL " + command + "\n"
        self.__douille.sendall(command)
        
        #debug
        print command

    def pjl_read(self, display=0):
        """Cette fonction lit le retour donn� par l'imprimante
        Elle retourne un message commen�ant par \"ERREUR :\" en
        cas d'�chec
        Elle affiche le message si la variable display est vraie
        """
        
        if not self.__douille:
            return "ERREUR : la connexion vers l'imprimante n'est pas ouverte"
        message=""
        caract=' '
        while caract:
            try:
                caract=self.__douille.recv(1)
                message+=caract
            except:
                caract=''
        message+='\n'
        if display:
            print message
        return message

    def write_file(self,filename):
        """Cette fonction envoie un fichier � l'imprimante.
        Elle est utile pour envoyer une s�rie de commande ou un fichier
        postscript.

        Arguments :
        filename : nom du fichier � envoyer
        """

        if not self.__douille:
            return "ERREUR : la connexion vers l'imprimante n'est pas ouverte"

        fichier=open(filename)
        self.__douille.sendall(fichier.read())
        fichier.close()

    def write_postscript(self,filename, name, display):
        """Cette fonction envoie un fichier postscript � l'imprimante.
        Elle envoie avant le ENTER LANGUAGE et le EOJ a la fin.

        Arguments :
        filename : nom du fichier � envoyer
        name : le nom du job
        display : ce qu'on met sur l'afficheur
        """
        if not self.__douille:
            return "ERREUR : la connexion vers l'imprimante n'est pas ouverte"
        self.pjl_command('JOB DISPLAY=\"%s\" NAME=\"%s\"' % (display,name))
        self.pjl_command('ENTER LANGUAGE = POSTSCRIPT ')
        self.write_file(filename)
        self.__douille.sendall(self.UEL+"@PJL EOJ NAME=\"%s\"\n" % (name))
        
        
    def __del__(self):
        """Destructeur : ferme la connexion"""
        self.cx_close()
        
