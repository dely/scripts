#!/usr/bin/env python
#
# DIALOGWIZARD.PY-- 
#
# Copyright (C) 2009 Antoine Durand-Gasselin
# Author: Antoine Durand-Gasselin <adg@crans.org>
#

from wizard import Step, PreviousStep, EndScenario, TryAgain
from itertools import izip
import dialog, time, sys, commands

def really_quit(dico):
    raise EndScenario("game over!", data = dico)

def error_handler(exc):
    dialog.Dialog().msgbox(str(exc), widht=0, height =0, title = 'Erreur :')
    raise TryAgain()

class DialogStepGenerator:
    u"""This class defines a step, that will prompt the user for various
    fields."""

    def __init__(self, backtitle):
        self.d = dialog.Dialog()
        self.d.add_persistent_args(["--backtitle", backtitle])

    def _check_rc(self, rc):
        if rc == 2: really_quit(dico)
        if rc == 1: raise PreviousStep

    def _skim_choices(self, choices, dico):
        return [ f for f in choices if f.get('show_cond', lambda x : True)(dico) ]

    def form_step(self, title, enonce, form, **kw):
        def fn(dico, default):
            sform = self._skim_choices(form, dico)
            for field in sform:
                item = default.get(field['var'], dico.get(field['var'], ''))
                field['item'] = item
            rc, res = self.d.form(enonce % dico, fields = sform, title=title % dico, **kw)
            self._check_rc(rc)
            for field, val in izip (sform, res):
                dico[field['var']] = val
            return dico
        return Step(fn)

    def select_step(self, title, enonce, var, choix, **kw):
        def fn(dico, default):
            schoix = []
            for c in choix:
                try:
                    schoix.append((c[0], c[1]))
                except Exception,e:
                    if c.get('show_cond', lambda x : True)(dico):
                        schoix.append((c['label'], c['item']))
            print schoix
            rc, res = self.d.menu(enonce % dico, choices = schoix, title = title % dico, **kw)
            self._check_rc(rc)
            dico[var] = res
            return dico
        return Step(fn)

    def input_step(self, title, enonce, var, **kw):
        def fn(dico, default):
            rc, res = self.d.inputbox(enonce % dico, title = title % dico, init = dico.get(var, ''), **kw)
            self._check_rc(rc)
            dico[var] = res
            return dico
        return Step(fn)

    def checklist_step(self, title, enonce, liste, **kw):
        def fn(dico, default):
            sliste = []
            for c in liste:
                try:
                    if dico.get(c[0], False): checked = 'on'
                    else: checked = 'off'
                    sliste.append((c[0], c[1], checked))
                except Exception,e:
                    if c.get('show_cond', lambda x : True)(dico):
                        if dico.get(c['var'], False): checked = 'on'
                        else: checked = 'off'
                        sliste.append((c['var'], c['item'], checked))
            rc, res = self.d.checklist(enonce % dico, title = title % dico, choices = sliste, **kw)
            self._check_rc(rc)
            for tag, item, status in liste:
                if tag in res:
                    dico[tag] = True
                else:
                    dico[tag] = False
            return dico
        return Step(fn)
