from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Credential(models.Model):
    service = models.CharField("Service", max_length=200)
    user = models.ForeignKey(User)
    token = models.CharField("Credential", max_length=1000, unique=True)
    date = models.DateTimeField(auto_now_add=True)
    class Admin:
        list_display = ('user', 'token', 'service', 'date')