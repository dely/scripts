from django.conf.urls.defaults import *

urlpatterns = patterns('',
    # Example:
    # (r'^test/', include('accounts.apps.foo.urls.foo')),
    (r'^$', 'accounts.cransaccounts.views.index'),
    (r'^test/$', 'accounts.test.views.my_view'),
    (r'^login/$', 'django.contrib.auth.views.login', {'template_name': 'login.html'}),
    (r'^auth/$', 'accounts.cransaccounts.views.auth'),

    (r'^logout/$', 'django.contrib.auth.views.logout_then_login'),
    # Uncomment this for admin:
    (r'^accounts/admin/', include('django.contrib.admin.urls')),
    (r'^mymedia/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': '/usr/scripts/accounts/media/',
         'show_indexes': True}
    ),
)
