from django.conf.urls.defaults import *

urlpatterns = patterns('',
    # Example:
    # (r'^test/', include('accounts.apps.foo.urls.foo')),
    (r'^verify/$', 'accounts.adm.views.check_credential'),
)
