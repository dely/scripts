
routeur -- Le pare-feu ipv4 de routeur
======================================

.. automodule:: gestion.gen_confs.firewall4.routeur
   :members:
   :special-members:
